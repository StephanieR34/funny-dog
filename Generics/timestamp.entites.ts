import { CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from "typeorm";

export class TimestampEntities {
    @CreateDateColumn(
        {
            update: false,
            select: false
        }
    )
    createAT: Date;

    @UpdateDateColumn({select: false})
    updateAT: Date;

    @DeleteDateColumn({select: false})
    deleteAT: Date;
}