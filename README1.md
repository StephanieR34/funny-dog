# Platforme de rencontre en proprietaire ou passionner d'animaux

## Idée générale:
permettre au personnes ayant des animaux (chien, chat, nac) de trouver 
d'autre passionné proche de chez eux pour pouvoir echanger et partager autour
d'une passion commune.

## Parcour utilisateur : 

(schema en cours)
```mermaid
graph TD
    A[page d'acceuil] -->|choix de la ville| B(Montpellier)
    A -->|choix de la ville| C(Sete)
    A -->|choix de la ville| D(Sommiere)
```

>une page d'acceuil qui presentent le fonctionnement de l'app avec :
>- un petit text explicatif, la liste des villes déjà renseigner et la liste des animaux représentés
>- une possibilité de se connecté si on a déjà un compte ou de  s'enregistrer

une fois logger on peu  choisir une race de chien puis une ville et ainsi voir une carte avec les autres utilisateurs enregistrer sur cette zone pour cette espece.

l'utilisateur peut alors proposer une activitée ou répondre a une activité proposé

En option si j'ai le temps ou en version deux la possibilité de mettre en place des messages privés et pourquoi un salon vocal...

## Flowchart

#### schema login

```mermaid
sequenceDiagram
    participant User
    participant ServeurOauth
    participant Api
    participant DB
    User->>ServeurOauth: demande token
    ServeurOauth->>Api: envoi objet json
    Api->>DB: remplissage DB table user
    DB->>User: Authentification ok
```
#### schema inscription sur la carte


