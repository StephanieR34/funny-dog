
INSERT INTO profil (nom, prenom, age, ville)
 VALUES
 ('Regnier', 'Stephanie', 'Montpellier', 39),
 ('Stelman', 'Lucile', 'Montpellier', 20),
 ('Richard', 'Nathan', 'Sete', 32),
 ('Delgadi', 'George', 'Montpellier', 55);

INSERT INTO chien (nom_chien, race_chien, age_chien, caractère_chien, path_photo_chien, profilId)
 VALUES 
 ('Daika', 'berger hollandais', 13, 'tres dominante envers les autres femelles', 'daika.jpg', 1),
 ('Joker', 'berger de shetland', 1, 'un peu pereux mais très sociable', 'joker.jpg', 1),
 ('Bali', 'berger hollandais', 5, 'supporte a peu pres tout les autres chiens qui savent arler chien', 'bali.jpg', 2),
 ('Balto', 'Husky', 3, 'Socialbe mais prefere courrir que jouer', 'Balto.jpg', 3),
 ('Wolf', 'Chien Loup tcheque', 6, 'fugueur ne peu etre détacher', 'wolf.jpg', 3),
 ('Satsu', 'Berger de shetland', 2, 'toujours partant pour un jeu entre copain', 'satsu.jpg', 4);