import { TimestampEntities } from 'Generics/timestamp.entites';
import { ChienEntity } from 'src/chien/entities/chien.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('profil')
export class ProfilsEntity extends TimestampEntities {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'nom',
    length: 45,
  })
  nom: string;

  @Column({
    length: 45,
  })
  prenom: string;

  @Column()
  age: number;

  @Column({
    length: 45,
  })
  ville: string;

  @OneToMany(() => ChienEntity, (chiens) => chiens.profil, {
    cascade: true,
    nullable: true,
    eager: true,
  })
  chiens: ChienEntity[];
}
