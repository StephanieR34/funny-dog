import { Body, Controller, Get, Post } from '@nestjs/common';
import { AddProfilsDto } from './dto/add-profils.dto';
import { ProfilsEntity } from './entities/profils.entity';
import { ProfilsService } from './profils.service';

@Controller('profils')
export class ProfilsController {
  constructor(private profilsService: ProfilsService) {}

  @Get()
  async getProfils(): Promise<ProfilsEntity[]> {
    return this.profilsService.getProfils();
  }

  // @Get('all')
  // async getAll() {
  //     return this.profilsService.getProfilsEtChiens();
  // }

  @Post()
  async addProfil(
    @Body() addProfilsDto: AddProfilsDto,
  ): Promise<ProfilsEntity> {
    return await this.profilsService.addProfils(addProfilsDto);
  }
}
