import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddProfilsDto } from './dto/add-profils.dto';
import { ProfilsEntity } from './entities/profils.entity';

@Injectable()
export class ProfilsService {
  constructor(
    @InjectRepository(ProfilsEntity)
    private profilRepository: Repository<ProfilsEntity>,
  ) {}
  async getProfils(): Promise<ProfilsEntity[]> {
    return await this.profilRepository.find();
  }

  async addProfils(profils: AddProfilsDto): Promise<ProfilsEntity> {
    return await this.profilRepository.save(profils);
  }

  // async getProfilsEtChiens() {
  //     // Creer un query builder
  //     const qd = this.profilRepository.createQueryBuilder('chien')
  //                 .innerJoin('profil','p','chien.profilId = p.id').getMany();

  //     console.log(qd)
  //     return qd

  // }
}
