import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class AddProfilsDto {
  @IsNotEmpty()
  @IsString()
  nom: string;

  @IsNotEmpty()
  @IsString()
  prenom: string;

  @IsNumber()
  age: number;

  @IsNotEmpty()
  @IsString()
  ville: string;
}
