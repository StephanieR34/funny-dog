import { Module } from '@nestjs/common';
import { ProfilsService } from './profils.service';
import { ProfilsController } from './profils.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProfilsEntity } from './entities/profils.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProfilsEntity])],
  providers: [ProfilsService],
  controllers: [ProfilsController],
})
export class ProfilsModule {}
