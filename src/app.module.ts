import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProfilsModule } from './profils/profils.module';
import { ChienModule } from './chien/chien.module';

import * as dotenv from 'dotenv';

dotenv.config();

@Module({
  imports: [
    ProfilsModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    ChienModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
