import { Body, Controller, Get, Post } from '@nestjs/common';
import { ChienService } from './chien.service';
import { AddChiensDto } from './dto/add-chiens.dto';
import { ChienEntity } from './entities/chien.entity';

@Controller('chien')
export class ChienController {
  constructor(private chienService: ChienService) {}
  @Post()
  async addChien(@Body() addChiensDto: AddChiensDto): Promise<ChienEntity> {
    return await this.chienService.addChien(addChiensDto);
  }
  @Get()
  async getChiens(): Promise<ChienEntity[]> {
    return this.chienService.getChiens();
  }
}
