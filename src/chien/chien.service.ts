import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddChiensDto } from './dto/add-chiens.dto';
import { ChienEntity } from './entities/chien.entity';

@Injectable()
export class ChienService {
  constructor(
    @InjectRepository(ChienEntity)
    private chienRepository: Repository<ChienEntity>,
  ) {}
  async addChien(chien: AddChiensDto): Promise<ChienEntity> {
    return await this.chienRepository.save(chien);
  }
  async getChiens(): Promise<ChienEntity[]> {
    return await this.chienRepository.find();
  }
}
