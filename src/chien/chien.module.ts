import { Module } from '@nestjs/common';
import { ChienService } from './chien.service';
import { ChienController } from './chien.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChienEntity } from './entities/chien.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ChienEntity])],
  providers: [ChienService],
  controllers: [ChienController],
})
export class ChienModule {}
