import { TimestampEntities } from 'Generics/timestamp.entites';
import { ProfilsEntity } from 'src/profils/entities/profils.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('chien')
export class ChienEntity extends TimestampEntities {
  @PrimaryGeneratedColumn()
  id_chien: number;

  @Column({
    length: 45,
  })
  nom_chien: string;

  @Column()
  race_chien: string;

  @Column()
  age_chien: number;

  @Column()
  caractere_chien: string;

  @Column()
  path_photo_chien: string;

  @Column()
  profilId: string;

  @ManyToOne(() => ProfilsEntity, (profil) => profil.chiens, {
    cascade: ['insert', 'update'],
    nullable: true,
  })
  profil: ProfilsEntity;
}
