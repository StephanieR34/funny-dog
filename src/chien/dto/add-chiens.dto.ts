import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class AddChiensDto {
  @IsNotEmpty()
  @IsNumber()
  profil_id: number;

  @IsNotEmpty()
  @IsString()
  nom_chien: string;

  @IsNotEmpty()
  @IsString()
  race_chien: string;

  @IsNumber()
  age_chien: number;

  @IsNotEmpty()
  @IsString()
  caractere_chien: string;

  @IsNotEmpty()
  @IsString()
  path_photo_chien: string;
}
